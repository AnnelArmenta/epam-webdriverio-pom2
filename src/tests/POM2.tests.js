const {pages} = require("./../po");

describe("New Paste test", ()=>{

    beforeEach(async ()=>{
        await pages('mainPage').open();
        await browser.maximizeWindow();
    });

    it("New Paste Bin with verification", async()=>{

        await pages('mainPage').postFields.item('postBox').click();
        await pages('mainPage').postFields.item('postBox').setValue('git config --global user.name  "New Sheriff in Town"\ngit reset $(git commit-tree HEAD^{tree} -m "Legacy code")\ngit push origin master --force');

        await browser.execute(()=>{
            highlighting = document.getElementById("postform-format");
            highlighting.style = "width: 1px; height: 1px; visibility: visible;"
            highlighting.aria_hidden = false; 
        })
        await pages('mainPage').postFields.syntaxHighlighting.selectByAttribute("value","8");
        await browser.pause(1000);

        await pages('mainPage').postFields.pasteExpirationViewable.scrollIntoView();
        await browser.execute(()=>{
            expiration = document.getElementById("postform-expiration");
            expiration.style = "width: 1px; height: 1px; visibility: visible;"
            expiration.aria_hidden = false;
        });
        await pages('mainPage').postFields.pasteExpiration.selectByAttribute("value","10M");
        await browser.pause(1000);

        await pages('mainPage').postFields.item('pasteName').click();
        await pages('mainPage').postFields.item('pasteName').setValue("how to gain dominance among developers");

        await browser.pause(1000);

        
        await pages('mainPage').postFields.newPasteBtn.click();
        await browser.pause(1000);

        //--------First validation----------

        const pasteBinName = "how to gain dominance among developers";
        const expectedTitle = pasteBinName + " - Pastebin.com";
        await expect(browser).toHaveTitle(expectedTitle);
        await browser.pause(1000);

        //-------Second validation---------
        
        await expect(pages('createdBinPage').createdBinDetails.syntaxHighlighting).toHaveText("Bash");

        //-------Third validation----------
        
        const code = await pages('createdBinPage').createdBinCode.fullCode;
        const expectedText = [
          'git config --global user.name  "New Sheriff in Town"',
          'git reset $(git commit-tree HEAD^{tree} -m "Legacy code")',
          'git push origin master --force'
        ];
    
        for (let i = 0; i < expectedText.length; i++) {
            const text = await code[i].getText();
            await expect(text).toEqual(expectedText[i]);
        }
      

    });
})

