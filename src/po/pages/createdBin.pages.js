const { BinCode, BinDetails } = require("./../components");
const BasePage = require("./basePage.pages.js");


class createdBinPage extends BasePage{

    constructor(){
        super('');
        this.createdBinCode = new BinCode();
        this.createdBinDetails = new BinDetails();
    }
    async open(){
        await browser.url('');
    }
}

module.exports = createdBinPage;