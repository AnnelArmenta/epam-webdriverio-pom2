const { NewPostBox, NewPostFields } = require("./../components");
const BasePage = require("./basePage.pages.js");

class MainPage extends BasePage{

    constructor(){
        super('');
        this.postBox = new NewPostBox();
        this.postFields = new NewPostFields();
    }
}

module.exports = MainPage;