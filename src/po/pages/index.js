const CreatedBinPage = require("./createdBin.pages.js");
const MainPage = require("./mainPage.pages.js");

/*
* @params page {'mainPage' | 'createdBinPage'}
* @returns {'MainPage' | 'CreatedBinPage'}
*/

function pages(page){
    const possiblePages = {
        mainPage: new MainPage(),
        createdBinPage: new CreatedBinPage()
    }
    return possiblePages[page];
}

module.exports = {
    MainPage,
    CreatedBinPage,
    pages,
}