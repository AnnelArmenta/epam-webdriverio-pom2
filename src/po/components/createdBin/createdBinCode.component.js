const baseComponent = require("./../common/baseElement.component.js");


class createdBinPageHeader extends baseComponent{

    constructor(){
        super("ol .bash");
    }

    get fullCode(){
        return $$('li[class="li1"]');
    }

}

module.exports = createdBinPageHeader;