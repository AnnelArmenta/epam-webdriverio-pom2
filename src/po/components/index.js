const BaseElement = require("./common/baseElement.component.js");

const BinCode = require("./createdBin/createdBinCode.component.js");
const BinDetails = require("./createdBin/createdBinDetails.component.js");

const NewPostBox = require("./newBin/newPost.component.js");
const NewPostFields = require("./newBin/newPostBottom.component.js");

module.exports = {
    BaseElement,
    BinCode,
    BinDetails,
    NewPostBox,
    NewPostFields
}