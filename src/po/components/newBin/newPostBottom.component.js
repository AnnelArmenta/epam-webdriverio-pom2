const baseComponent = require("./../common/baseElement.component.js");

class NewPostFields extends baseComponent{

    constructor(){
        super('#w0');
    }

    item(param){
        const selector = {
            postBox:'//*[@id="postform-text"]',
            tags: '.bootstrap-tagsinput',
            pasteName: '//*[@id="postform-name"]'
        }
        return this.rootEl.$(selector[param]);
    }

    get syntaxHighlighting(){
        return $('#postform-format');
    }

    get categories(){
        return $('//*[@id="select2-postform-category_id-container"]');
    }

    
    get pasteExpirationViewable(){
        return $('//*[@id="select2-postform-expiration-container"]');
    }

    get pasteExpiration(){
        return $('#postform-expiration');
    }

    get pasteExposure(){
        return $('//*[@id="select2-postform-status-container"]');
    }

    get newPasteBtn(){
        return $('button.btn.-big');
    }
}

module.exports = NewPostFields;