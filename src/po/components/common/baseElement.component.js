class baseComponent{

    constructor(rootSelector){
        this.rootSelector = rootSelector;
    }

    get rootEl(){
        return $(this.rootSelector);
    }

}

module.exports = baseComponent;